<?php

if (!defined('_PS_VERSION_'))
    exit;

class BlocktopmenuOverride extends Blocktopmenu
{
    public function install($delete_params = true)
    {
        if (!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayTop') ||
            !$this->registerHook('actionObjectCategoryUpdateAfter') ||
            !$this->registerHook('actionObjectCategoryDeleteAfter') ||
            !$this->registerHook('actionObjectCategoryAddAfter') ||
            !$this->registerHook('actionObjectCmsUpdateAfter') ||
            !$this->registerHook('actionObjectCmsDeleteAfter') ||
            !$this->registerHook('actionObjectCmsAddAfter') ||
            !$this->registerHook('actionObjectSupplierUpdateAfter') ||
            !$this->registerHook('actionObjectSupplierDeleteAfter') ||
            !$this->registerHook('actionObjectSupplierAddAfter') ||
            !$this->registerHook('actionObjectManufacturerUpdateAfter') ||
            !$this->registerHook('actionObjectManufacturerDeleteAfter') ||
            !$this->registerHook('actionObjectManufacturerAddAfter') ||
            !$this->registerHook('actionObjectProductUpdateAfter') ||
            !$this->registerHook('actionObjectProductDeleteAfter') ||
            !$this->registerHook('actionObjectProductAddAfter') ||
            !$this->registerHook('categoryUpdate') ||
            !$this->registerHook('displayHomeNav') ||
            !$this->registerHook('actionShopDataDuplication')
        ) {
            return false;
        }

        $this->clearMenuCache();

        if ($delete_params) {
            if (!$this->installDb() || !Configuration::updateGlobalValue('MOD_BLOCKTOPMENU_ITEMS', 'CAT3,CAT26') || !Configuration::updateGlobalValue('MOD_BLOCKTOPMENU_SEARCH', '1')) {
                return false;
            }
        }

        return true;
    }

    public function hookHeader()
    {

    }

    public function hookDisplayHomeNav($params)
    {
        return parent::hookDisplayTop($params);
    }

    protected function generateCategoriesMenu($categories, $is_children = 0)
    {
        $html = '';

        foreach ($categories as $key => $category) {
            if ($category['level_depth'] > 1) {
                $cat = new Category($category['id_category']);
                $link = Tools::HtmlEntitiesUTF8($cat->getLink());
            } else {
                $link = $this->context->link->getPageLink('index');
            }

            /* Whenever a category is not active we shouldnt display it to customer */
            if ((bool)$category['active'] === false) {
                continue;
            }
            if (isset($category['children']) && !empty($category['children'])) {
                $html .= '<li' . (($this->page_name == 'category'
                        && (int)Tools::getValue('id_category') == (int)$category['id_category']) ? ' class="sfHoverForce"' : '') .
                    ' class="level0 level-top parent">';
            } else {
                $html .= '<li' . (($this->page_name == 'category'
                        && (int)Tools::getValue('id_category') == (int)$category['id_category']) ? ' class="sfHoverForce"' : '') .
                    ' class="level0 level-top">';
            }
            $html .= '<a href="' . $link . '" title="' . $category['name'] . '" class="level-top"><span>' . $category['name'] . '</span></a>';

            if (isset($category['children']) && !empty($category['children'])) {
                $html .= '<ul>';
                foreach ($category['children'] as $key1 => $child) {
                    if ($child['level_depth'] > 1) {
                        $cat_child = new Category($child['id_category']);
                        $link_child = Tools::HtmlEntitiesUTF8($cat_child->getLink());
                    } else {
                        $link_child = $this->context->link->getPageLink('index');
                    }
                    $html .= '<li>';
                    $html .= ' <a href="' . $link_child . '"><span>' . $child['name'] . '</span></a>';
                    $html .= '</li>';
                }


                $html .= '</ul>';
            }

            $html .= '</li>';
        }

        return $html;
    }
}