<div class="header-search">
    <form id="searchbox" method="get"
          action="{$link->getPageLink('search', null, null, null, false, null, true)|escape:'html':'UTF-8'}">
        <input type="hidden" name="controller" value="search"/>
        <input type="hidden" name="orderby" value="position"/>
        <input type="hidden" name="orderway" value="desc"/>
        <fieldset>
            <label class="header-search-label" for="search">Search site:</label>
            <div class="header-search-field-wrapper">
                <input class="header-search-field" id="search_query_top" type="search" name="search_query" value=""
                        maxlength="128" placeholder="Search"/>
                <button type="submit" title="{l s='Search' mod='blocksearch'}" name="submit_search" class="header-search-btn icon-search"><span
                            class="is-vishidden">{l s='Search' mod='blocksearch'}</span></button>
            </div>
        </fieldset>
    </form>
</div>