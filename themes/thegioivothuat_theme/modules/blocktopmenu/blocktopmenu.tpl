{if $MENU != ''}
<nav class="nav-primary">
    <ul class="content-wrap nav-list dl-menu">
        {$MENU}
        <div class="back-shim"></div>
    </ul>
</nav>
{/if}