<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_859e85774d372c6084d62d02324a1cc3'] = 'Sản phẩm đã chọn';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_eaa362292272519b786c2046ab4b68d2'] = 'Thêm một khối hiển thị các sản phẩm đã xem.';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_2e57399079951d84b435700493b8a8c1'] = 'Bạn cần điền vào trường \"Sản phẩm đã hiển thị\".';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_73293a024e644165e9bf48f270af63a0'] = 'Số không hợp lệ.';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_f38f5974cdc23279ffe6d203641a8bdf'] = 'Cập nhật cấu hình.';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'Tùy chỉnh';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_26986c3388870d4148b1b5375368a83d'] = 'Những sản phẩm trưng bày';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_d36bbb6066e3744039d38e580f17a2cc'] = 'Xác định số sản phẩm hiển thị trong khối này.';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'Lưu';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_43560641f91e63dc83682bc598892fa1'] = 'Sản phẩm đã xem';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_8f7f4c1ce7a4f933663d10543562b096'] = 'Chi tiết';
$_MODULE['<{blockviewed}thegioivothuat_theme>blockviewed_c70ad5f80e4c6f299013e08cabc980df'] = 'Thông tin thêm về %s';
