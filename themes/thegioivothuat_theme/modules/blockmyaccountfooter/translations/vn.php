<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_b97d23f7cde011d190f39468e146425e'] = 'Khối \"tài khoản của tôi\" ở dưới phía dưới website';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_abdb95361b4c92488add0a5a37afabcb'] = 'Hiển thị block với các liên kết liên quan đến tài khoản người dùng ';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_ae9ec80afffec5a455fbf2361a06168a'] = 'Quản lý tài khoản khách hàng của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'My Account';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_74ecd9234b2a42ca13e775193f391833'] = 'My Orders';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_5973e925605a501b18e48280f04f0347'] = 'Hàng hóa trả lại của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_89080f0eedbd5491a93157930f1e45fc'] = 'Hàng hóa trả lại của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_9132bc7bac91dd4e1c453d4e96edf219'] = 'Biên lai Ghi có của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Địa chỉ của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_b4b80a59559e84e8497f746aac634674'] = 'Quản lý thông tin cá nhân của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_63b1ba91576576e6cf2da6fab7617e58'] = 'Thông tin cá nhân';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_95d2137c196c7f84df5753ed78f18332'] = 'Mã giảm giá của tôi';
$_MODULE['<{blockmyaccountfooter}thegioivothuat_theme>blockmyaccountfooter_c87aacf5673fada1108c9f809d354311'] = 'Đăng xuất';
