/*
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2016 PrestaShop SA
 *  @version  Release: $Revision$
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

$(document).ready(function () {

    if (typeof(homesliderslick_loop) == 'undefined')
        homesliderslick_loop = true;
    if (typeof(homesliderslick_speed) == 'undefined')
        homesliderslick_speed = 1000;
    if (typeof(homesliderslick_pause) == 'undefined')
        homesliderslick_pause = 4000;

    else
        if(homesliderslick_loop == 1)
            homesliderslick_loop = true;
        else
            homesliderslick_loop = false;


    var heroBanners = $('.header-after-container ul');
    var numHeroBanners = heroBanners.children('li').length;
    var heroSlickOptions = {
        slidesToShow: 1,
        autoplay: homesliderslick_loop,
        autoplaySpeed: homesliderslick_speed,
        speed: homesliderslick_pause,
        arrows: true,
        dots: true
    };

    // Only enable slider bleed on edges if there is more than one banner
    if (numHeroBanners > 1) {
        heroBanners.addClass('slick-multiple');
        heroSlickOptions.centerMode = true;
        heroSlickOptions.centerPadding = '5%';
    }

    heroBanners.slick(heroSlickOptions);
});